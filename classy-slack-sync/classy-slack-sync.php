<?php

/*
  Plugin Name: Classy Slack Sync
  Description: Syncs Classy donation to Slack as a cron
  Version: 0.0.3
  Author: Neal Fennimore
  Author URI: http://neal.codes
  License: GPL V3
 */

if ( ! defined( 'WPINC' ) ) {
  die;
}

class Slack_Sync {
  private static $instance = null,
                 $baseURL = SLACK_DONATION_URL,
                 $timeDelay = 2700;

  public static function init() {
    if ( self::$instance == null ) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  private function __construct() {
    add_filter( 'cron_schedules', array( $this, 'forty_five_minutes' ) );
    add_action( 'slack_cron', array( $this, 'execute' ) );

    register_activation_hook( __FILE__, array( $this, 'activate' ) );
    register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
  }

  public function activate() {
    if ( !class_exists('Classy_API') ) {
      deactivate_plugins( plugin_basename( __FILE__ ) );
      wp_die( 'This plugin requires the Classy API plugin to be installed and activated.' );
    }

    $timestamp = wp_next_scheduled( 'slack_cron' );
    if( $timestamp == false ){
      wp_schedule_event( time(), 'forty_five_minutes', 'slack_cron' );
    }
  }

  public function deactivate() {
    wp_clear_scheduled_hook( 'slack_cron' );
  }

  public function execute() {
    $donations = self::get_donations();
    if (empty($donations)) { return; }
    $data = self::format_data($donations);
    self::send_to_slack($data);
  }

  private function format_data($donations){
    $data = array();
    foreach ($donations as $donation) {

      $donate_amount = $donation['donate_amount'];
      $billing_name = $donation['billing_name'];
      $recurring_id = $donation['recurring_id'];
      $designation_name = $donation['designation_name'];
      $event_name = $donation['event_name'];
      $email = $donation['email'];
      $comment = $donation['comment'];

      if ($comment == "") {
        $text = "_New Donation_";
      } else {
        $text = "_" . $comment . "_";
      }

      if ($recurring_id == 0) {
        $pretext = "*$" . $donate_amount . "* for {$designation_name} ({$event_name})";
      } else {
        $pretext = "*$" . $donate_amount . "* _recurring_ for {$designation_name} ({$event_name})";
      }

      array_push($data, array(
                            "fallback" => "New $" . $donate_amount . " donation from {$billing_name}",
                            "pretext" => $pretext,
                            "title" => $billing_name,
                            "title_link"=> "mailto:{$email}?subject=Thank you",
                            "text"=> $text,
                            "color"=> "#2BB5BD",
                            "mrkdwn_in"=> array("text", "pretext")
                            ));
    }
    return array("attachments" => $data);
  }

  private function send_to_slack($data){
    $ch = curl_init( self::$baseURL . '?' );
    $payload = json_encode( $data );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    $result = curl_exec($ch);
    curl_close($ch);
  }

  private function get_donations(){
    date_default_timezone_set('America/Chicago'); // Classy server is Central time
    $start = date('Y-m-d H:i:s', time() - self::$timeDelay);
    $end = date('Y-m-d H:i:s', time());
    $donations = Classy_API::get_donations(array('start_date' => $start, 'end_date' => $end));
    return $donations;
  }

  /**
   * [forty_five_minutes Custom cron time]
   * @param  [type] $schedules [description]
   * @return [type]            [description]
   */
  public function forty_five_minutes( $schedules ) {
    $schedules['forty_five_minutes'] = array(
      'interval' => self::$timeDelay, //2700
      'display' => 'Every forty-five minutes'
    );
    return $schedules;
  }

}

Slack_Sync::init();