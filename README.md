#StayClassy Slack Wordpress Plugin
Donation bot for [Slack](https://www.slack.com) using the [Classy](https://www.classy.org) API. You'll need our [StayClassy API Plugin](https://github.com/Samasource/wp-stay-classy-api). 

![Classy Slack Donation Bot](/../screenshots/screenshots/slack-bot.png "Classy Slack Donation Bot")

## Setup 
Add the `classy-slack-sync` directory to `wp-content/plugins`. (`wp-content/plugins/classy-slack-sync`)

Create an Incoming WebHook integration for Slack. Get your Webhook URL and add it as SLACK_DONATION_URL in your Wordpress `wp-config.php` file.

```php
define('SLACK_DONATION_URL', 'xxxxxxxxxxxxxxxxxxxxx');
```

Activate the plugin in your Wordpress Dashboard and the plugin will act as a cron, getting any new donations (currently set to every 45 minutes), and posting them to the channel you chose when you set up the webhook.

It lists the donations as attachments, with each donors name acting as a mailto: link for easy email creation. 

## Wordpress Cron

Wordpress' wp-cron (`wp-cron.php`) is not a real cron and only activates when a user actually visits the website. If no one visits the site, the cron will never run. To ensure that the wp-cron runs you'll have it up as a real cron using the server, and run it every minute or so. 

```sh
*/1 * * * * curl -I https://example.org/wp-cron.php
```

